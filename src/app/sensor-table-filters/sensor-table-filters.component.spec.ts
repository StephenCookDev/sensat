import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorTableFiltersComponent } from './sensor-table-filters.component';

describe('SensorTableFiltersComponent', () => {
  let component: SensorTableFiltersComponent;
  let fixture: ComponentFixture<SensorTableFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SensorTableFiltersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorTableFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
