import { Pipe, PipeTransform } from '@angular/core';

const getMedian = (dataList: number[]): number => {
  const sortedDataList = dataList.sort();
  const middleIndex = Math.floor(dataList.length / 2);

  return sortedDataList[middleIndex];
};

@Pipe({ name: 'aggregate' })
export class AggregatePipe implements PipeTransform {
  transform<T extends Object>(
    dataList: T[],
    groupField: string,
    dataField: string,
    enabled: boolean
  ): T[] {
    if (!enabled) return dataList;

    const dataListByGroupField = {};
    dataList.forEach((entry) => {
      const groupFieldValue = entry[groupField];
      dataListByGroupField[groupFieldValue] =
        dataListByGroupField[groupFieldValue] || [];
      dataListByGroupField[groupFieldValue].push(entry);
    });

    const newDataList = [];
    Object.keys(dataListByGroupField).forEach((groupFieldValue) => {
      const allEntries = dataListByGroupField[groupFieldValue];

      const newEntry = allEntries[0];
      newEntry[dataField] = getMedian(
        allEntries.map((entry) => entry[dataField])
      );

      newDataList.push(newEntry);
    });

    return newDataList;
  }
}
