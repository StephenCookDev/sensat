# Sensat Test

## How To Run

To run this code, first install dependencies using npm:

```
npm install
```

and then start the server

```
npm start
```

and finally, opening the development server on `http://localhost:4200`

## Technical Decisions / Captain's Log

This repo was initially generated using `ng new sensat`. It has been cut back a
bit, for ease of review, but some bootstrapping comments may still exist in
places.

I created a separate SensorTableFilters component, bound to the fields in the
SensorTable component. In hind-sight, I don't think this is the cleanest way to
achieve this separation (I think I'm treating the data in a React-y way) — and
probably the correct way to achieve this would be to create a proper form model.

In a production environment, I would consider leveraging a library like
ng2-table, or ag-grid. However, for the purposes of the test (and without of the
constraints of reality, e.g. deployment environment, future extensibility, etc.)
— I decided to write some basic pipes to handle the data transformation myself.

I have pulled in CDK's virtual scrolling functionality as there was too much
data for Angular to be able to render performantly, otherwise. Pagination could
also work here, and depending on the use-cases involved could make for a better
UX. However, for the use-case of just grepping through the data, one large list
is a better UX (and easier to implement, to boot).

I have styled everything a bit, but haven't focused on making anything
_beautiful_. Again, in production, depending on the use-case this might be
something we would want to really polish.

Finally, I have omitted unit testing for time here. Given more time, the first
things I would unit-test are the pipes, as these deal with pure data
transformation, and are thus ideal candidates for unit testing. Component-level
unit-testing is more flakey, so I would write fewer tests for the components,
but would still like to test the entrypoints of each component at the very
least.
