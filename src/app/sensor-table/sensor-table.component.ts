import { Component, OnInit } from '@angular/core';
import sensorReadings from '../../assets/sensor_readings.json';

@Component({
  selector: 'app-sensor-table',
  templateUrl: './sensor-table.component.html',
  styleUrls: ['./sensor-table.component.css'],
})
export class SensorTableComponent implements OnInit {
  public readings;

  public sortBy = 'sensor_type';
  public sortDirection = 'asc';

  public filterBy = 'name';
  public filterSearchTerm = '';

  public shouldAggregate = false;

  constructor() {}

  ngOnInit(): void {
    // On init we set the `readings` member to match the JSON blob — however,
    // the code is written to assume that `readings` can be asynchronously
    // populated at any time, to allow for easy addition of pulling from an API.
    const pretendToBeApiDelay = 0;

    setTimeout(() => {
      this.readings = sensorReadings;
    }, pretendToBeApiDelay);
  }

  sortByChange(sortBy: string): void {
    this.sortBy = sortBy;
    this.hackForceRerender();
  }

  sortDirectionChange(sortDirection: 'asc' | 'desc'): void {
    this.sortDirection = sortDirection;
    this.hackForceRerender();
  }

  filterByChange(event): void {
    this.filterBy = event.target.value || 'name';
  }

  filterSearchTermChange(event: any): void {
    this.filterSearchTerm = event.target.value || '';
  }

  shouldAggregateChange(event: any): void {
    this.shouldAggregate = event.target.checked || false;
  }

  private hackForceRerender() {
    // It looks as if the CDK Virtual Scroll module has some issues with
    // tracking the reordering of its input list. This is either a bug on their
    // side, or a misunderstanding on my part of exactly how its API is intended
    // to work. Given more time, I would dig further into this to get a better
    // understanding of the full issue. For now, by creating a new array
    // reference, the virtual scroll component sees a "new" data entry, and thus
    // re-renders correctly
    this.readings = [...this.readings];
  }
}
