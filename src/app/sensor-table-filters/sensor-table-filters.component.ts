import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sensor-table-filters',
  templateUrl: './sensor-table-filters.component.html',
  styleUrls: ['./sensor-table-filters.component.css'],
})
export class SensorTableFiltersComponent implements OnInit {
  public sort;

  @Input() sortBy: string;
  @Output() sortByChange: EventEmitter<string> = new EventEmitter<string>();
  @Input() sortDirection: 'asc' | 'desc';
  @Output() sortDirectionChange: EventEmitter<
    'asc' | 'desc'
  > = new EventEmitter<'asc' | 'desc'>();

  @Input() filterBy: string;
  @Output() filterByChange: EventEmitter<string> = new EventEmitter<string>();
  @Input() filterSearchTerm: string;
  @Output() filterSearchTermChange: EventEmitter<string> = new EventEmitter<
    string
  >();

  @Input() shouldAggregate: boolean;
  @Output() shouldAggregateChange: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();

  constructor() {}

  ngOnInit(): void {
    this.sort = `${this.sortBy}___${this.sortDirection}`;
  }

  handleSort(event): void {
    this.sort = event.target.value;
    const [sortBy, sortDirection] = this.sort.split('___');

    this.sortByChange.emit(sortBy);
    this.sortDirectionChange.emit(sortDirection);
  }
}
