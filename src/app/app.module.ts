import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SensorTableComponent } from './sensor-table/sensor-table.component';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { OrderByPipe } from './order-by.pipe';
import { FilterPipe } from './filter.pipe';
import { AggregatePipe } from './aggregate.pipe';
import { SensorTableFiltersComponent } from './sensor-table-filters/sensor-table-filters.component';

@NgModule({
  declarations: [
    AppComponent,
    SensorTableComponent,
    LoadingSpinnerComponent,
    OrderByPipe,
    FilterPipe,
    AggregatePipe,
    SensorTableFiltersComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, ScrollingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
