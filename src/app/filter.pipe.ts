import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'filter' })
export class FilterPipe implements PipeTransform {
  transform<T extends Object>(dataList: T[], field: string, val: string): T[] {
    /**
     * This pipe filters on a case-insensitive basis, but given more time would
     * ideally fuzzy-search using some third party library
     */
    return dataList.filter((entry) => {
      return entry[field].toLowerCase().includes(val.toLowerCase());
    });
  }
}
