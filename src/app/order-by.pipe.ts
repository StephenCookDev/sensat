import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'orderBy' })
export class OrderByPipe implements PipeTransform {
  transform<T extends Object>(
    dataList: T[],
    field: string,
    direction: 'asc' | 'desc'
  ): T[] {
    return dataList.sort((a, b) => {
      const directionMultiplier = direction === 'asc' ? 1 : -1;
      const sortVal = a[field].localeCompare(b[field]);

      return sortVal * directionMultiplier;
    });
  }
}
